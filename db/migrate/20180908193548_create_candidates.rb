class CreateCandidates < ActiveRecord::Migration[5.2]
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :career_level
      t.integer :years_of_experience
      t.text :cover_note
      t.string 'skills', array: true

      t.timestamps
    end
  end
end
