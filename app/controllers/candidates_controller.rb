class CandidatesController < ApplicationController
  def new
    @candidate = Candidate.new
  end

  def create
    @candidate = Candidate.new(candidate_params)

    if @candidate.save
      redirect_to @candidate
    else
      render 'new'
    end
  end

  def show
    @candidate = Candidate.find(params[:id])
  end


  private

  def candidate_params
    params.require(:candidate).permit(:name, :email, :phone, :career_level, :years_of_experience, :cover_note, :avatar, skills: [])
  end
end
