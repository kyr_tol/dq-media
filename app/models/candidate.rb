class Candidate < ApplicationRecord
  has_one_attached :avatar

  validates_presence_of :name
  validates_format_of :email, with: URI::MailTo::EMAIL_REGEXP, presence: true
  validates_format_of :phone, :with => /\w/, allow_blank: true
  validates_inclusion_of :career_level, in: ['Student', 'Graduate', 'Entry Level', 'Experienced', 'Executive', 'Senior Executive'],
                         message: "%{value} is not a valid career level"
  validates_numericality_of :years_of_experience, only_integer: true, allow_blank: true
  validates_length_of :cover_note, maximum: 200, allow_blank: true
  validates_format_of :avatar_attachment, :with => /\.(jpeg|jpg|png|gif)$/i, :multiline => true, allow_blank: true
  validate :validate_skills

  private

  def validate_skills
    errors.add(:skills, 'are less than 3') unless skills.reject {|e| e.to_s.empty?}.length >= 3
  end

end
